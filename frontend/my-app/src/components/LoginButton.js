import React from 'react';
import Button from 'react-bootstrap/Button';

const LoginButton = () => {
    
    return <Button variant="dark" href="/login">  Login </Button>
}
export default LoginButton;